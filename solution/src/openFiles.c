#include "bmp.h"
#include <stdio.h>



void fileRead(char** in, struct image *img) {
    FILE* inFile = fopen(*in, "rb"); 
    from_bmp(inFile, img);
    //printf("%d\n", result);
    fclose(inFile);
}

void fileWrite(char** out, struct image *img) {
    FILE* outFile = fopen(*out, "wb");
    to_bmp(outFile, img);
    //printf("%d\n", result);
    fclose(outFile);
}

void run(char** in, char** out) {
    struct image img;
    fileRead(in, &img);
    img = rotate(img);
    //printf("%d_%d\n", img.height, img.width);
    fileWrite(out, &img);

}
