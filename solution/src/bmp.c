#include "image.h"
#include <malloc.h>
#include <stdint.h>
#include <stdio.h>



#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    size_t count = fread(&header, sizeof(struct bmp_header), 1, in);
    if (count != 1) {
        return READ_INVALID_HEADER;
    }
    img->height = header.biHeight;
    img->width = header.biWidth;
    printf("%d__%d\n", header.biHeight, header.biWidth);
    img->data = malloc(img->height * img->width * sizeof(struct pixel));
    for (int i = 0; i < img->height; i++) {
        count = fread(img->data + i*img->width, 1, img->width * sizeof(struct pixel), in);
        if (count != sizeof(struct pixel) * img->width) {
            return READ_INVALID_SIGNATURE;
        }
        if (count % 4 != 0) {
            size_t padding = 4 - count % 4;
            fseek(in, (long) padding, 1);
        }
    }
    return READ_OK;

}

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum write_status to_bmp( FILE* out, struct image const* img ) {
    struct bmp_header header;
    size_t padding = 0;
    size_t count = img->width * sizeof(struct pixel) % 4;
    if (count != 0) {
        padding = 4 - count % 4;
    }
    header.biHeight = img->height;
    header.biWidth = img->width;
    header.bfType = 19778;
    header.bfReserved = 0;
    header.bOffBits = sizeof(struct bmp_header);
    header.biSize = 40;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = (img->width + padding) * img->height * sizeof(struct pixel);
    header.bfileSize = header.bOffBits + header.biSizeImage;
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed =0;
    header.biClrImportant = 0;

    count = fwrite(&header, 1, sizeof(struct bmp_header), out);
    if (count != sizeof(struct bmp_header) ){
        return WRITE_ERROR;
    }
    uint8_t paddingCount = 0;
    for (int i = 0; i < img->height; i++) {
        count = fwrite(img->data + i*img->width, 1, img->width * sizeof(struct pixel), out);
        if (count != sizeof(struct pixel) * img->width) {
            return WRITE_ERROR;
        }
        for (size_t j = padding; j > 0; j--) {
            fwrite(&paddingCount, sizeof(uint8_t), 1, out);
        }
    }
    free(img->data);
    return WRITE_OK;
}
