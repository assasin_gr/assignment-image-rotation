#include <stdint.h>
#include <stdlib.h>

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct pixel { uint8_t b, g, r; };

struct image rotate( struct image const source ) {
    struct image newImage;
    newImage.height = source.width;
    newImage.width = source.height;
    newImage.data = malloc(newImage.height * newImage.width * sizeof(struct pixel));
    for (int j = (int)source.width - 1; j >= 0; j--) {
        for (int i = 0; i < source.height; i++) {
            newImage.data[j*source.height +  source.height - 1 -i] = source.data[i*source.width + j];
        }
    }
    free(source.data);
    return newImage;
}

