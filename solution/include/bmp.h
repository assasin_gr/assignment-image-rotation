
#ifndef UNTITLED_BMP_H
#define UNTITLED_BMP_H

#include <stdio.h>
#include  <stdint.h>
#include <malloc.h>
#include "image.h"

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
};
enum write_status to_bmp( FILE* out, struct image const* img );
enum read_status from_bmp( FILE* in, struct image* img );

#endif //UNTITLED_BMP_H
